from tkinter import *
import requests
from bs4 import BeautifulSoup

class Aplikasi(Frame):

        def __init__(self,master):
                Frame.__init__(self,master)
                self.grid()
                self.create_widgets()

        def create_widgets(self):
                self.instruction=Label(self, text="Masukkan kata!!")
                self.instruction.grid(row=0,column=0,columnspan=2,sticky=W)

                self.masukkan=Entry(self)
                self.masukkan.grid(row=1,column=1,sticky=W)

                self.submit_button=Button(self,text="Submit",command=self.reveal)
                self.submit_button.grid(row=2,column=0,sticky=W)

                self.text=Text(self,width=35,height=5,wrap=WORD)
                self.text.grid(row=3,column=0,columnspan=2,sticky=W)

        def reveal(self):
                word=self.masukkan.get()

                if word != "":
                        url="http://www.kamus.net/indonesia/"+word
                        r=requests.get(url)
                        soup=BeautifulSoup(r.text,"lxml")
                        divTran=soup.find("div",{"class":"trans-target"})
                        isi=divTran.find("strong")
                        hasil=isi.text
                        message=hasil

                else:
                        message="Masukkan kata"
                self.text.insert(0.0,message)

root=Tk()
root.title("Mini Kamus")
root.geometry("250x200")
app=Aplikasi(root)

root.mainloop()

